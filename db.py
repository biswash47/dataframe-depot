from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, insert, select
import pandas as pd
from typing import Dict


class Db:
    _DEFAULT_DF_INDEX = "__df_index__"
    _METADATA_TABLE_NAME = "metadata_table"

    def __init__(self, conn_str="sqlite://"):
        self.engine = create_engine(conn_str)
        self._metadata = MetaData(bind=self.engine)
        self._table_count = 0

        self._create_metadata_table()

    @property
    def _curr_table(self):
        curr = self._curr_table
        self._table_count += 1
        return curr

    def _create_metadata_table(self):
        """
        A metadata sql table to keep track of dataframes' names and indices.
        :return:
        """
        Table(self._METADATA_TABLE_NAME,
              self._metadata,
              Column('id', Integer, autoincrement=True, primary_key=True),
              Column('df_table_name', String, nullable=False, unique=True),
              Column('df_index_name', String, nullable=False))
        self._metadata.create_all(bind=self.engine)

    def read_table_as_df(self, table_name: str):
        df_index = self._get_df_index(table_name)
        df = pd.read_sql_table(table_name, self.engine, index_col=df_index)

        if df_index == self._DEFAULT_DF_INDEX:
            df.index.name = None
        return df

    def query(self, query_str: str):
        return pd.read_sql(query_str, self.engine)

    def _get_df_index(self, table_name):
        metadata_table = self._all_tables[self._METADATA_TABLE_NAME]
        stmt = select(metadata_table.c.df_index_name).where(metadata_table.c.df_table_name == table_name)
        with self.engine.connect() as conn:
            # needs *_ here because .one() returns a tuple
            df_index, *_ = conn.execute(stmt).one()
        return df_index

    def save_df(self, df, table_name=None):
        table_name = table_name or f"__table__{self._curr_table}"
        index_name = df.index.name or self._DEFAULT_DF_INDEX
        df.to_sql(table_name, self.engine, index_label=index_name)

        self._save_metadata(table_name=table_name, index_name=index_name)

    def _save_metadata(self, /, table_name: str, index_name: str):
        metadata_table = self._all_tables[self._METADATA_TABLE_NAME]
        stmt = insert(metadata_table).values(
            df_table_name=table_name,
            df_index_name=index_name)

        with self.engine.connect() as conn:
            conn.execute(stmt)

    @property
    def _all_tables(self) -> Dict[str, Table]:
        self._metadata.reflect(bind=self.engine)
        return self._metadata.tables

    @property
    def tables(self):
        df_tables = (tab for tab in self._all_tables.keys() if tab not in {self._METADATA_TABLE_NAME})
        yield from df_tables
