from typing import Set

import pandas as pd
from db import Db


class DfDepot:
    def __init__(self, /, conn_str=None, **dfs):
        self._df_names = set()
        self.db = Db()
        self.save(**dfs)

        # TODO: db instance from conn_str

    @property
    def _dfs(self):
        return self._df_names

    @property
    def _num_dfs(self):
        return len(self._dfs)

    def save(self, **dfs):
        validate_df_types_names(dfs, self._df_names)
        # update keys
        self._df_names = self._df_names | dfs.keys()

        for name, df in dfs.items():
            print(name)
            self.db.save_df(df, table_name=name)

    def get(self, name: str):
        self.db.read_table_as_df(name)

    def query(self, query_str: str):
        if query_str in self._df_names:
            return self.get(query_str)
        return self.db.query(query_str)

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        del self


def validate_df_types_names(df_dict, existing_names: Set[str]):
    err_msg = []
    for name, df in df_dict.items():
        if name in existing_names:
            err_msg.append(f"Name {name!r} already exists.")

        if not isinstance(df, pd.DataFrame):
            err_msg.append(
                f"Expected type {pd.DataFrame!r} for {name!r}. Got {type(df)!r}"
            )
    if len(err_msg) > 0:
        raise ValueError("\nDuplicate names or invalid datatypes.\n" + '\n'.join(err_msg))
